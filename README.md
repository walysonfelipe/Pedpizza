<p align="center">
     <img  src="https://github.com/walysonfelipe/Pedpizza/blob/master/__system__/img/logo.png?raw=true" align="center" width="100px">
</p>  

##  Índice
- [Introdução](#-introdução)
- [Tecnologia Utilizadas](#-tecnologia-utilizadas)
- [Testar Aplicação](#-testar-aplicação)
- [Novas Features](#-novas-features)

## 📕 Introdução
[Em breve]

## 👨‍💻 Tecnologia Utilizadas


<table>
  <tr>
    <th><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/php/php.png"></th>
    <th><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mysql/mysql.png"></th>
    <th><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></th>
    <th><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png"></th>
    <th><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png"></th>
 </tr>
</table>


## 🚀 Testar Aplicação
[Em breve]

## 👨‍💻 Novas Features

- Mudando as cores 
- Reformulução Administrador



  
Criado por [Walyson Felipe](https://github.com/walysonfelipe) Copyright 2019-2020 🚀.
